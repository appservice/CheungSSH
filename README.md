# CheungSSH 4.0
## 作者  ：张其川
#### 性能和稳定上有了极大的提升，欢迎更新使用，如果有使用问题，欢迎加作者QQ咨询解决。
#### 如果解压失败，请您重新下载即可：
#### QQ群：445342415
##### 管理员账户：cheungssh
##### 管理员密码: cheungssh
---
#### Docker版本安装方法，请使用如下命令：
1. docker  pull registry.cn-hangzhou.aliyuncs.com/cheungssh/cheungssh 
2. docker  run -tid  --name cheungssh  -v 8099:80 registry.cn-hangzhou.aliyuncs.com/cheungssh/cheungssh
3. 用浏览器打开: http://您的虚拟机地址:8099 即可使用，如: http://192.168.1.1:9090  使用方法请参见文档《CheungSSH3.0使用手册.docx》
4. 如果有任何问题，欢迎加群：445342415 解决
